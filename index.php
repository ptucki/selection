<?php

require 'vendor/autoload.php';

use Selection\Analysis\PayloadGenerator;
use Selection\Analysis\Visualizer;
use Selection\Core\Worker;

const PRODUCTS_COUNT = 15;
const STORES_COUNT = 5;

$payloadGenerator = new PayloadGenerator();
$payload = $payloadGenerator->generate(PRODUCTS_COUNT, STORES_COUNT);

$worker1 = new Worker(isset($_GET['a']) ? $_GET['a'] : 'random', $payload);
$worker2 = new Worker(isset($_GET['b']) ? $_GET['b'] : 'cheapest', $payload);

$visualizer = new Visualizer();
print $visualizer->toHtml($payload, $worker1, $worker2);
