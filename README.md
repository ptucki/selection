# Price comparison algorithm

This project contains a framework to develop and compare solutions of solving a computationally hard task of selecting stores to deliver a certain group of products, so that:

1. the number of deliveries is minimal,
2. the total order price (products and deliveries cost) is minimal.

Heuristic iterative strategy yields very good results in reasonable time.

# Quickstart

1. Install `php7.4-cli`, `php7.4-mbstring` and `php7.4-gmp`.
2. Install [Composer](https://getcomposer.org/download/) PHP package manager. 
3. Load project dependencies with `composer install`.
4. Start built-in PHP webserver with `php -S localhost:8000`.
5. Open `http://localhost:8000` in your browser.
