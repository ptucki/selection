<?php

namespace Selection\Analysis;

use Selection\Core\Payload;

class PayloadGenerator
{
    private const DELIVERY_PRICE_MAX = 4000;
    private const DELIVERY_PRICE_MIN = 800;
    private const PRODUCT_AVAILABLE_MINIMUM = 2;
    private const PRODUCT_PRICE_MAX = 20000;
    private const PRODUCT_PRICE_MEAN_DEVIATION = 0.15;
    private const PRODUCT_PRICE_MIN = 100;

    public function generate(int $productsCount, int $storesCount): Payload
    {
        if ($productsCount <= 0) {
            throw new \InvalidArgumentException('Products count must be positive.');
        }
        if ($storesCount <= 0) {
            throw new \InvalidArgumentException('Stores count must be positive.');
        }

        $storeIds = range(0, $storesCount - 1);
        $productIds = range(0, $productsCount - 1);
        $payload = new Payload();

        foreach ($storeIds as $storeId) {
            $payload->deliveryTable[$storeId] = random_int(self::DELIVERY_PRICE_MIN, self::DELIVERY_PRICE_MAX);
        }

        foreach ($productIds as $productId) {
            $meanPrice = random_int(self::PRODUCT_PRICE_MIN, self::PRODUCT_PRICE_MAX);
            $minPrice = round($meanPrice * (1 - self::PRODUCT_PRICE_MEAN_DEVIATION));
            $maxPrice = round($meanPrice * (1 + self::PRODUCT_PRICE_MEAN_DEVIATION));

            foreach ($storeIds as $storeId) {
                $payload->offerTable[$productId][$storeId] = null;
            }

            $availableCount = random_int(self::PRODUCT_AVAILABLE_MINIMUM, $storesCount);
            $productStoreIds = $storeIds;

            while ($availableCount) {
                $randomStoreKey = array_rand($productStoreIds);
                $storeId = $productStoreIds[$randomStoreKey];
                unset($productStoreIds[$randomStoreKey]);
                $payload->offerTable[$productId][$storeId] = random_int($minPrice, $maxPrice);
                $availableCount--;
            }
        }

        return $payload;
    }
}