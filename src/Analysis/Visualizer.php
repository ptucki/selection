<?php

namespace Selection\Analysis;

use Selection\Core\Payload;
use Selection\Core\Worker;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFilter;

class Visualizer
{
    public function formatCost(?int $cost): ?string
    {
        return $cost
            ? str_replace('.', ',', bcdiv($cost, 100, 2))
            : '–';
    }

    public function formatTime(?float $time): ?string
    {
        return $time
            ? str_replace('.', ',', number_format($time, 4))
            : '–';
    }

    public function toHtml(Payload $payload, Worker $worker1, Worker $worker2): string
    {
        $loader = new FilesystemLoader(__DIR__ . '/../templates');
        $twig = new Environment($loader);
        $twig->addFilter(new TwigFilter('format_cost', [$this, 'formatCost']));
        $twig->addFilter(new TwigFilter('format_time', [$this, 'formatTime']));
        $template = $twig->load('solution.html');
        return $template->render(compact('payload', 'worker1', 'worker2'));
    }
}
