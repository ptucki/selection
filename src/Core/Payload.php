<?php

namespace Selection\Core;

// Models input data for a problem of finding a smallest and cheapest combination of stores which will deliver all products in demand.
class Payload
{
    /** @var array [ int store id => int delivery cost in cents ] */
    public array $deliveryTable = [];
    /** @var array [ int product id => int store id => int offer cost in cents | null ] */
    public array $offerTable = [];
}