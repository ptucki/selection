<?php

namespace Selection\Core;

// Models solution finding strategy interface for a problem of finding a smallest and cheapest combination of stores which will deliver all products in demand.
interface Strategy
{
    public function getName(): string;
    public function solve(Payload $payload): Solution;
}
