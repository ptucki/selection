<?php

namespace Selection\Core;

use Selection\Core\Strategy\Cheapest\Cheapest;
use Selection\Core\Strategy\Iterative\BruteForce;
use Selection\Core\Strategy\Iterative\Heuristic;
use Selection\Core\Strategy\Random\Random;

class Worker
{
    private Solution $solution;
    private Strategy $strategy;
    private float $time;

    public function __construct(string $strategy, Payload $payload) {
        switch ($strategy) {
            case 'brute_force':
                $this->strategy = new BruteForce();
            break;
            case 'cheapest':
                $this->strategy = new Cheapest();
            break;
            case 'heuristic':
                $this->strategy = new Heuristic();
            break;
            case 'random':
                $this->strategy = new Random();
            break;
            default:
                throw new \InvalidArgumentException("Strategy '{$strategy}' is not supported.");
        }
        $this->solve($payload);
    }

    public function getDeliveryCost(Payload $payload): int {
        $sum = 0;
        $storeIds = array_values(array_unique($this->solution->assignment));
        foreach ($storeIds as $storeId) {
            $sum += $payload->deliveryTable[$storeId];
        }
        return $sum;
    }

    public function getName(): string {
        return $this->strategy->getName();
    }

    public function getProductCost(Payload $payload): int {
        $sum = 0;
        foreach ($this->solution->assignment as $productId => $storeId) {
            $sum += $payload->offerTable[$productId][$storeId];
        }
        return $sum;
    }

    public function getStoreCount(): int {
        return count(array_unique($this->solution->assignment));
    }

    public function getSolution(): Solution {
        return $this->solution;
    }

    public function getTime(): float {
        return $this->time;
    }

    public function getTotalCost(Payload $payload): int {
        return $this->getProductCost($payload) + $this->getDeliveryCost($payload);
    }

    private function solve(Payload $payload): void {
        $start = microtime(true);
        $this->solution = $this->strategy->solve($payload);
        $this->time = microtime(true) - $start;
    }
}
