<?php

namespace Selection\Core;

// Models output data for a problem of finding a smallest and cheapest combination of stores which will deliver all products in demand.
class Solution
{
    /** @var array [ int product id => int store id ] */
    public array $assignment;
}
