<?php

namespace Selection\Core\Strategy\Random;

use Selection\Core\Payload;
use Selection\Core\Solution;
use Selection\Core\Strategy;

class Random implements Strategy
{
    public function getName(): string {
        return 'Random';
    }

    public function solve(Payload $payload): Solution {
        $solution = new Solution();
        foreach ($payload->offerTable as $productId => $pricesByStoreId) {
            $solution->assignment[$productId] = array_rand(array_filter($pricesByStoreId));
        }
        return $solution;
    }
}
