<?php

namespace Selection\Core\Strategy\Iterative;

class BruteForce extends Iterative
{
    public function getName(): string {
        return 'Brute Force';
    }

    protected function optimize(): void {
        // Do not optimize the set, take brute force approach.
    }
}
