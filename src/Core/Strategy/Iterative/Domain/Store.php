<?php

namespace Selection\Core\Strategy\Iterative\Domain;

class Store
{
    private int $id;
    private int $deliveryCost;
    private array $offerTable;
    private Supply $supply;

    /** @param [ int product id => int offer cost in cents ] $offerTable */
    public function __construct(int $id, array $offerTable, int $deliveryCost)
    {
        $this->id = $id;
        $this->offerTable = $offerTable;
        $this->deliveryCost = $deliveryCost;

        $availabilitySequence = implode('', array_map(
            fn (?int $cost) => is_int($cost) ? '1' : '0',
            $offerTable,
        ));
        $this->supply = Supply::fromBitString($availabilitySequence);
    }

    public function getSupply(): Supply
    {
        return $this->supply;
    }

    public function getId(): int
    {
        return $this->id;
    }
}