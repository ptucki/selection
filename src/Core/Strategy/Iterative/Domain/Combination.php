<?php

namespace Selection\Core\Strategy\Iterative\Domain;

class Combination
{
    private Supply $supply;
    /** @var [ store id => store id ] in ascending order. */
    private array $storeIds = [];

    public function __construct(Store $store)
    {
        $this->storeIds[$store->getId()] = $store->getId();
        $this->supply = $store->getSupply();
    }

    public function __clone()
    {
        $this->supply = clone $this->supply;
    }

    public function __toString(): string
    {
        return implode(',', $this->storeIds);
    }

    public function add(Store $store): self
    {
        if (isset($this->storeIds[$store->getId()])) {
            throw new \InvalidArgumentException("Store {$store->getId()} is already present in the combination.");
        }
        $this->storeIds[$store->getId()] = $store->getId();
        asort($this->storeIds);
        $this->supply->add($store->getSupply());
        return $this;
    }

    public function getStoreIds(): array
    {
        return $this->storeIds;
    }

    public function getSupply(): Supply
    {
        return $this->supply;
    }
}
