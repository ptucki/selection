<?php

namespace Selection\Core\Strategy\Iterative\Domain;

use Selection\Core\Payload;

class CombinationTransformer
{
    public function getAssignment(Combination $combination, Payload $payload): array
    {
        $assignment = [];
        foreach ($payload->offerTable as $productId => $costsByStores) {
            $lowestCost = INF;
            foreach ($combination->getStoreIds() as $storeId) {
                $cost = $costsByStores[$storeId];
                if ($cost && $cost < $lowestCost) {
                    $lowestCost = $cost;
                    $assignment[$productId] = $storeId;
                }
            }
        }
        return $assignment;
    }

    public function getCost(Combination $combination, Payload $payload): int
    {
        $cost = 0;
        $assignment = $this->getAssignment($combination, $payload);
        foreach ($assignment as $productId => $storeId) {
            $cost += $payload->offerTable[$productId][$storeId];
        }
        foreach ($combination->getStoreIds() as $storeId) {
            $cost += $payload->deliveryTable[$storeId];
        }
        return $cost;
    }
}
