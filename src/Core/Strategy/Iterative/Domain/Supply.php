<?php

namespace Selection\Core\Strategy\Iterative\Domain;

class Supply
{
    public static function fromBitString(string $bits): Supply
    {
        return new self(gmp_init($bits, 2));
    }

    private int $ones;
    private \GMP $sequence;

    public function __construct(\GMP $sequence)
    {
        $this->sequence = $sequence;
        $this->ones = gmp_popcount($this->sequence);
    }

    public function getSequence(): \GMP
    {
        return $this->sequence;
    }

    public function getCount(): int
    {
        return $this->ones;
    }

    public function add(Supply $supply): self
    {
        $this->sequence = gmp_or($this->sequence, $supply->getSequence());
        $this->ones = gmp_popcount($this->sequence);
        return $this;
    }
}
