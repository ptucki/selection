<?php

namespace Selection\Core\Strategy\Iterative\Domain;

use Selection\Core\Payload;

class Multipliers
{
    /** @var int[][] store id lists sorted by product supply count - store ids under key 0 supply rarest product etc. */
    private array $multipliers;

    /** @var Store[] keyed by store id. */
    private array $stores = [];

    public function __construct(Payload $payload)
    {
        foreach ($payload->deliveryTable as $storeId => $cost) {
            $storeOfferTable = array_map(
                fn(array $costsByStores) => $costsByStores[$storeId],
                $payload->offerTable,
            );
            $this->stores[$storeId] = new Store($storeId, $storeOfferTable, $cost);
        }
        $this->multipliers = array_map(
            fn($costsByStores) => array_keys(array_filter($costsByStores)),
            $payload->offerTable,
        );
        usort(
            $this->multipliers,
            fn(array $productSuppliers1, array $productSuppliers2) => count($productSuppliers1) - count($productSuppliers2)
        );
    }

    public function getInitial(): array {
        return array_map(
            fn(int $storeId) => $this->getStore($storeId),
            $this->multipliers[0],
        );
    }

    /** @param [ store id => store id ] $stores */
    public function getMultipliers(array $stores): array
    {
        $multipliers = [];
        for ($i = count($stores); $i < count($this->multipliers); $i++) {
            $finished = true;
            foreach ($this->multipliers[$i] as $store) {
                if (isset($multipliers[$store])) {
                    continue;
                }
                if (isset($stores[$store])) {
                    $finished = false; // Continue searching for multipliers at the next level.
                } else {
                    $multipliers[$store] = $store;
                }
            }
            if ($finished) {
                break;
            }
        }
        return array_map(
            fn(int $storeId) => $this->getStore($storeId),
            $multipliers,
        );
    }

    private function getStore(int $id): Store
    {
        return $this->stores[$id];
    }
}
