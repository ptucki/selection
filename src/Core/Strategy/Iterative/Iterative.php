<?php

namespace Selection\Core\Strategy\Iterative;

use Selection\Core\Payload;
use Selection\Core\Solution;
use Selection\Core\Strategy;
use Selection\Core\Strategy\Iterative\Domain\Combination;
use Selection\Core\Strategy\Iterative\Domain\CombinationTransformer;
use Selection\Core\Strategy\Iterative\Domain\Multipliers;
use Selection\Core\Strategy\Iterative\Domain\Store;

abstract class Iterative implements Strategy
{
    /** @var Combination[] of same size. */
    protected array $set;

    private CombinationTransformer $combinationTransformer;
    private Multipliers $multipliers;
    protected int $productsCount;
    protected int $storesCount;
    private int $size;

    abstract protected function optimize(): void;

    public function solve(Payload $payload): Solution {
        $this->combinationTransformer = new CombinationTransformer;
        $this->multipliers = new Multipliers($payload);
        $this->productsCount = count($payload->offerTable);
        $this->storesCount = count($payload->deliveryTable);

        $this->initialize();

        while ( ! $this->containsCompleteCombinations()) {
            $this->increment();
        }

        $winner = $this->getCheapestCombination($payload);

        $solution = new Solution();

        $solution->assignment = $this->combinationTransformer->getAssignment($winner, $payload);

        return $solution;
    }

    private function containsCompleteCombinations(): bool {
        foreach ($this->set as $combination) {
            if ($combination->getSupply()->getCount() === $this->productsCount) {
                return true;
            }
        }
        return false;
    }

    private function increment(): void {
        $this->size++;
        foreach ($this->set as $key => $combination) {
            unset($this->set[$key]);
            $stores = $this->multipliers->getMultipliers($combination->getStoreIds());
            foreach ($stores as $store) {
                try {
                    $newCombination = (clone $combination)->add($store);
                    $this->set[(string) $newCombination] = $newCombination;
                } catch (\Exception $e) {}
            }
        }
        $this->optimize();
    }

    private function initialize(): void {
        $this->set = array_map(
            fn(Store $store) => new Combination($store),
            $this->multipliers->getInitial(),
        );
        $this->size = 1;
    }

    private function getCheapestCombination(Payload $payload): Combination {
        $cheapestKey = null;
        $cheapestCost = INF;
        foreach ($this->set as $key => $combination) {
            if ($combination->getSupply()->getCount() < $this->productsCount) {
                continue;
            }
            $cost = $this->combinationTransformer->getCost($combination, $payload);
            if ($cost < $cheapestCost) {
                $cheapestKey = $key;
                $cheapestCost = $cost;
            }
        }
        return $this->set[$cheapestKey];
    }
}
