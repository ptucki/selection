<?php

namespace Selection\Core\Strategy\Iterative;

class Heuristic extends Iterative
{
    private const MAX_SET_SIZE = 2500;

    public function getName(): string {
        return 'Heuristic';
    }

    protected function optimize(): void {
        if (count($this->set) <= self::MAX_SET_SIZE) {
            return;
        }
        $minSupplyCount = INF;
        $maxSupplyCount = -INF;
        foreach ($this->set as $combination) {
            $supplyCount = $combination->getSupply()->getCount();
            if ($supplyCount < $minSupplyCount) {
                $minSupplyCount = $supplyCount;
            }
            if ($supplyCount > $maxSupplyCount) {
                $maxSupplyCount = $supplyCount;
            }
        }
        $supplyCumulativeHistogram = array_combine(
            range($minSupplyCount, $maxSupplyCount),
            array_fill($minSupplyCount, $maxSupplyCount - $minSupplyCount + 1, 0),
        );
        foreach ($this->set as $combination) {
            $supplyCount = $combination->getSupply()->getCount();
            for ($i = $minSupplyCount; $i <= $supplyCount; $i++) {
                $supplyCumulativeHistogram[$i]++;
            }
        }
        for ($i = $maxSupplyCount; $i >= $minSupplyCount; $i--) {
            $sum = $supplyCumulativeHistogram[$i];
            if ($sum >= self::MAX_SET_SIZE) {
                $supplyCountThreshold = $i;
                break;
            }
        }
        $supplyCountThresholdToRemove = $supplyCumulativeHistogram[$supplyCountThreshold] - self::MAX_SET_SIZE;
        foreach ($this->set as $key => $combination) {
            $supplyCount = $combination->getSupply()->getCount();
            if ($supplyCount > $supplyCountThreshold) {
                continue;
            }
            if ($supplyCount === $supplyCountThreshold && $supplyCountThresholdToRemove-- <= 0) {
                continue;
            }
            unset($this->set[$key]);
        }
    }
}
