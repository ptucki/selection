<?php

namespace Selection\Core\Strategy\Cheapest;

use Selection\Core\Payload;
use Selection\Core\Solution;
use Selection\Core\Strategy;

class Cheapest implements Strategy
{
    public function getName(): string {
        return 'Cheapest';
    }

    public function solve(Payload $payload): Solution {
        $solution = new Solution();
        foreach ($payload->offerTable as $productId => $costsByStores) {
            $lowestCost = INF;
            $lowestCostStoreId = null;
            foreach ($costsByStores as $storeId => $cost) {
                if ($cost && $cost < $lowestCost) {
                    $lowestCost = $cost;
                    $lowestCostStoreId = $storeId;
                }
            }
            $solution->assignment[$productId] = $lowestCostStoreId;
        }
        return $solution;
    }
}
